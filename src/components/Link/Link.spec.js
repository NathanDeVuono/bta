/* eslint-disable */
import React from 'react';
import renderer from 'react-test-renderer';
import Link from './Link';

it('Renders', () => {
  const tree = renderer.create(<Link to="/" />).toJSON();
  expect(tree).toMatchSnapshot();
})