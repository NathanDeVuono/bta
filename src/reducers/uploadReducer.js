import { uploadActions } from 'actions';

export default function uploadReducer(state = {}, action) {
  switch (action.type) {
    case uploadActions.TYPES.FILE_RECIEVED: {
      return {
        ...state,
        file: action.payload,
      };
    }
    default:
      return state;
  }
}
