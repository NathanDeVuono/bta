export function getActions(actionsWithEnum) {
  const actions = {
    ...actionsWithEnum,
  };
  delete actions.TYPES;
  return actions;
}
