import toRegex from 'path-to-regexp';

function matchURI(path, uri) {
  const keys = [];
  const pattern = toRegex(path, keys); // TODO: Use caching
  const match = pattern.exec(uri);
  if (!match) return null;
  const params = Object.create(null);
  for (let i = 1; i < match.length; i + 1) {
    params[keys[i - 1].name] =
      match[i] !== undefined ? match[i] : undefined;
  }
  return params;
}

export function resolve(routes, context, children) {
  const route = routes.find(r => context.pathname === r.path);
  if (route !== '/error') {
    const uri = context.error ? '/error' : context.pathname;
    const params = matchURI(route.path, uri);
    if (params) {
      const result = route.action({ ...context, params, children });
      if (result) {
        return result;
      }
    }
  }
  const error = new Error('Route not found');
  error.status = 404;
  throw error;
}
