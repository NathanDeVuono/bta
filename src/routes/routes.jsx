import React from 'react';
import {
  Home,
  Page,
  Tree,
} from 'views';

/* eslint-disable react/prop-types */
export default [
  { path: '/', action: () => (<Page><Home /></Page>) },
  { path: '/tree', action: () => (<Page><Tree /></Page>) },
];
