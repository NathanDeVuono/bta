import React, { PropTypes } from 'react';

export default function Page({ children }) {
  return (
    <div>
      <header>
        <h1>Holy shit we did it</h1>
      </header>
      <main>
        {children}
      </main>
    </div>
  );
}

Page.propTypes = {
  children: PropTypes.node,
};

Page.defaultProps = {
  children: null,
};
