/* eslint-disable */
import React from 'react';
import renderer from 'react-test-renderer';
import Page from './Page';

it('Renders', () => {
  const tree = renderer.create(<Page />).toJSON();
  expect(tree).toMatchSnapshot();
})

it('Renders with children', () => {
  const tree = renderer.create(<Page>I'm a child element!</Page>).toJSON();
  expect(tree).toMatchSnapshot();
})