import React, { PropTypes } from 'react';
import { history } from 'util';

import { Button } from 'components';

const navigateToTree = () => history.push({ pathname: 'tree' });

export default function Home({ fileRecieved }) {
  return (
    <section>
      Home is here
      <label htmlFor="file-upload">
        <p>Upload a behavior tree here</p>
        <input type="file" id="file-upload" onChange={fileRecieved} />
      </label>
      <p>Or start a</p>
      <div>
        <Button onClick={navigateToTree}>New Tree</Button>
      </div>
    </section>
  );
}

Home.propTypes = {
  fileRecieved: PropTypes.func.isRequired,
};
