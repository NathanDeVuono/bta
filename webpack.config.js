const resolve = require('path').resolve;
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = env => ({
  devtool: 'source-map',
  entry: {
    vendor: [
      'react',
      'react-dom',
      'd3-hierarchy',
    ],
    main: './src',
  },
  output: {
    path: resolve(__dirname, 'dist'),
    filename: '[chunkhash].[name].js',
  },
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          'style-loader',
          'css-loader',
          'sass-loader',
        ],
      },
      {
        test: /\.jsx?$/,
        use: 'babel-loader',
        exclude: /node_modules/,
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'Behavior Trees',
      hash: true,
      cache: true,
      template: resolve(__dirname, 'src/index.html'),
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: ['vendor', 'manifest'],
      minChunks: module => module.context && module.context.indexOf('node_modules') !== -1,
    }),
  ],
  resolve: {
    extensions: ['.js', '.jsx', '.json'],
    alias: {
      actions: resolve(__dirname, 'src/actions'),
      components: resolve(__dirname, 'src/components'),
      routes: resolve(__dirname, 'src/routes'),
      reducers: resolve(__dirname, 'src/reducers'),
      util: resolve(__dirname, 'src/util'),
      views: resolve(__dirname, 'src/views'),
    },
  },
});
